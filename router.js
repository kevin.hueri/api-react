/*
 * Import Module
 * ************* */ 

const express = require('express')
const router = express.Router()

// Controllers
const NewsControllers = require('./controllers/newsController')
const TownsControllers = require('./controllers/townsController')

// MIddleware
const MDTest = require('./middleware/test')

/*
 * Routes
 * ****** */ 

// '/'

// Route News '/'
router
  .get("/news", NewsControllers.getAll)
  .get("/news/:id", NewsControllers.getId)
  .post("/news", NewsControllers.create)
  .put("/news/:id", NewsControllers.editOne)
  .delete("/news", NewsControllers.deleteAll)
  .delete("/news/:id", NewsControllers.deleteOne)

  .get("/towns", TownsControllers.getAll)
  .get("/towns/:ville", TownsControllers.getOneTowns)
  .post("/towns", TownsControllers.create)
  .put("/towns/:ville", TownsControllers.editOne)
  .delete("/towns", TownsControllers.deleteAll)
  .delete("/towns/:ville", TownsControllers.deleteOne)

module.exports = router