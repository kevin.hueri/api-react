/*
 * News Controllers
 * ****************** */

// Import Model
const News = require("../models/NewsModel");



// GetAll
exports.getAll = async (req, res) => {

    // Recupération des données dans la DB
    const dbNews = await News.find();

    // Résultat dans le json
    res.json({
        message: "Voici vos données News",
        dbNews
    });
};



// GetOne
exports.getId = async (req, res) => {

    // Recupération des données selectoinnées dans la DB
    const newsID = await News.findById(req.params.id);

    // Résultat dans le json
    res.json({
        message: "Voici votre News ID",
        newsID
    });

}



// Create
exports.create = async (req, res) => {
    console.log('create news back: ', req.body)

    // Variable pour diminuer la suite des requetes
    const b = req.body;

    // Condition pour vérifier si nous avons un name et un message dans le body
    if (b.author && b.content) {

        // On définit la construction de notre article
        const news = new News({
            author: b.author,
            content: b.content,
            date: Date.now()
        });

        // Et on sauvegarde nos modifications
        news.save((err) => {});

        // Recupération des données dans la DB
        const dbNews = await News.find();

        // Resultat affiché sur Postman lors de la création
        res.json({
            message: "Item crée avec succes !",
            dbNews
        });

        // Resultat affiché sur Postman lors de l'aboutissement de la requete
    } else res.json({
        message: "Erreur, l'item n'as pas été créé !"
    });
};



// EditOne
exports.editOne = async (req, res) => {
    // console.log('editOne', req.params)

    // Récupération des données séléctionnées et modification de celles ci
    await News.findByIdAndUpdate(req.params.id, {
        ...req.body
    })

    // Résultat renvoyé
    res.json({
        message: "Item edité avec succes !"
    });
};



// DeleteAll
exports.deleteAll = async (req, res) => {

    // Recupération des données dans la DB
    await News.find();

    // Suppression de la totalité de la table News
    await News.deleteMany();

    // Resultat affiché sur Postman lors de l'aboutissement de la requete
    res.json({
        message: "Tous les items ont été supprimés avec succes !"
    });
};



// DeleteOne
exports.deleteOne = async (req, res) => {

    console.log('DeleteOne News', res.params)

    // Recupération des données sélectionnées dans la DB
    const NewsId = await News.findById(req.params.id);

    // Suppression de l'id de la DB
    await News.findByIdAndDelete(NewsId);

    // Recupération des données dans la DB
    const dbNews = await News.find();

    // Resultat affiché sur Postman lors de l'aboutissement de la requete
    res.json({
        message: "L'item a été supprimé avec succes !",
        dbNews
    });
}