/*
 * Towns Controllers
 * ****************** */

// Import Model
const Towns = require("../models/TownsModel");

// GetAll
exports.getAll = async (req, res) => {
    const dbTowns = await Towns.find();
    res.json({
        message: "Voici vos villes",
        dbTowns
    });
};

// GetOne
exports.getOneTowns = async (req, res) => {
    const TownsID = await Towns.findOne({ ville: req.params.ville });
    res.json({
        message: "Voici votre ville ID",
        TownsID
    });

}

// Create
exports.create = async(req, res) => {
    const b = req.body;
    if (b.ville) {
        const cTown = new Towns({
            ville: b.ville
        });
        cTown.save((err) => {});
        const dbTowns = await Towns.find();

        res.json({
            message: "Ville crée avec succes !",
            dbTowns
        });
    } else res.json({
        message: "Error, la ville n'as pas été créée !"
    });
};

// EditOne
exports.editOne = async (req, res) => {
    await Towns.findByIdAndUpdate(req.params.id, {...req.body})
    res.json({
        message: "Ville editée avec succes !"
    });
};

// DeleteAll
exports.deleteAll = async (req, res) => {
    await Towns.find();
    await Towns.deleteMany();
    res.json({
        message: "Toutes les villes ont été supprimées avec succes !"
    });
};

// DeleteOne
exports.deleteOne = async (req, res) => {
    await Towns.findOneAndDelete({ ville: req.params.ville });
    const dbTowns = await Towns.find();
    res.json({
        message: "La ville a été supprimée avec succes !",
        dbTowns
    });
}