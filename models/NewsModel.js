/*
 *
 * Model de 'News'
 ******************************/

// Import de Mongoose
const mongoose = require('mongoose')
const Schema = mongoose.Schema


// Création de notre Shéma (Model)
// c'est le Model de (Model)
const NewsSchema = new Schema({
    // Première variable (basique)
    author: {
        type: String,
        default: "User"
    },
    content: {
        type: String,
        default: 'Comment'
    },
    date: Date
})

// Et l'on export notre model grace à la passerelle Mongoose
// Ce qui nous permettra de pouvoir l'utiliser sur d'autre page
module.exports = mongoose.model('News', NewsSchema)