/*
 *
 * Model de 'Towns'
 ******************************/

// Import de Mongoose
const mongoose = require('mongoose')
const Schema = mongoose.Schema


const TownsSchema = new Schema({
    ville: {
        type: String,
        unique: true
    }
})


module.exports = mongoose.model('Towns', TownsSchema)